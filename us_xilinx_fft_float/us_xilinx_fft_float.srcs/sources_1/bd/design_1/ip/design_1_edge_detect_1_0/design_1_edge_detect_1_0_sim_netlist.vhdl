-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
-- Date        : Mon Nov 12 14:13:04 2018
-- Host        : Shears running 64-bit Ubuntu 16.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_1_edge_detect_1_0 -prefix
--               design_1_edge_detect_1_0_ design_1_edge_detect_1_0_sim_netlist.vhdl
-- Design      : design_1_edge_detect_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xczu9eg-ffvb1156-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_edge_detect_1_0_edge_detect is
  port (
    edge_detected : out STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    clk : in STD_LOGIC
  );
end design_1_edge_detect_1_0_edge_detect;

architecture STRUCTURE of design_1_edge_detect_1_0_edge_detect is
  signal edge_detected_i_i_10_n_0 : STD_LOGIC;
  signal edge_detected_i_i_11_n_0 : STD_LOGIC;
  signal edge_detected_i_i_12_n_0 : STD_LOGIC;
  signal edge_detected_i_i_13_n_0 : STD_LOGIC;
  signal edge_detected_i_i_2_n_0 : STD_LOGIC;
  signal edge_detected_i_i_3_n_0 : STD_LOGIC;
  signal edge_detected_i_i_4_n_0 : STD_LOGIC;
  signal edge_detected_i_i_5_n_0 : STD_LOGIC;
  signal edge_detected_i_i_6_n_0 : STD_LOGIC;
  signal edge_detected_i_i_7_n_0 : STD_LOGIC;
  signal edge_detected_i_i_8_n_0 : STD_LOGIC;
  signal edge_detected_i_i_9_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal tmp : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
edge_detected_i_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => edge_detected_i_i_2_n_0,
      I1 => edge_detected_i_i_3_n_0,
      I2 => edge_detected_i_i_4_n_0,
      I3 => edge_detected_i_i_5_n_0,
      O => p_0_in
    );
edge_detected_i_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(12),
      I1 => din(12),
      I2 => din(14),
      I3 => tmp(14),
      I4 => din(13),
      I5 => tmp(13),
      O => edge_detected_i_i_10_n_0
    );
edge_detected_i_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(15),
      I1 => din(15),
      I2 => din(17),
      I3 => tmp(17),
      I4 => din(16),
      I5 => tmp(16),
      O => edge_detected_i_i_11_n_0
    );
edge_detected_i_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(3),
      I1 => din(3),
      I2 => din(5),
      I3 => tmp(5),
      I4 => din(4),
      I5 => tmp(4),
      O => edge_detected_i_i_12_n_0
    );
edge_detected_i_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(0),
      I1 => din(0),
      I2 => din(2),
      I3 => tmp(2),
      I4 => din(1),
      I5 => tmp(1),
      O => edge_detected_i_i_13_n_0
    );
edge_detected_i_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(27),
      I1 => din(27),
      I2 => din(29),
      I3 => tmp(29),
      I4 => din(28),
      I5 => tmp(28),
      O => edge_detected_i_i_2_n_0
    );
edge_detected_i_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(24),
      I1 => din(24),
      I2 => din(26),
      I3 => tmp(26),
      I4 => din(25),
      I5 => tmp(25),
      O => edge_detected_i_i_3_n_0
    );
edge_detected_i_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => edge_detected_i_i_6_n_0,
      I1 => edge_detected_i_i_7_n_0,
      I2 => edge_detected_i_i_8_n_0,
      I3 => edge_detected_i_i_9_n_0,
      I4 => edge_detected_i_i_10_n_0,
      I5 => edge_detected_i_i_11_n_0,
      O => edge_detected_i_i_4_n_0
    );
edge_detected_i_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF6FF6"
    )
        port map (
      I0 => din(31),
      I1 => tmp(31),
      I2 => din(30),
      I3 => tmp(30),
      I4 => edge_detected_i_i_12_n_0,
      I5 => edge_detected_i_i_13_n_0,
      O => edge_detected_i_i_5_n_0
    );
edge_detected_i_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(9),
      I1 => din(9),
      I2 => din(11),
      I3 => tmp(11),
      I4 => din(10),
      I5 => tmp(10),
      O => edge_detected_i_i_6_n_0
    );
edge_detected_i_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(6),
      I1 => din(6),
      I2 => din(8),
      I3 => tmp(8),
      I4 => din(7),
      I5 => tmp(7),
      O => edge_detected_i_i_7_n_0
    );
edge_detected_i_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(18),
      I1 => din(18),
      I2 => din(20),
      I3 => tmp(20),
      I4 => din(19),
      I5 => tmp(19),
      O => edge_detected_i_i_8_n_0
    );
edge_detected_i_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6FF6FFFFFFFF6FF6"
    )
        port map (
      I0 => tmp(21),
      I1 => din(21),
      I2 => din(23),
      I3 => tmp(23),
      I4 => din(22),
      I5 => tmp(22),
      O => edge_detected_i_i_9_n_0
    );
edge_detected_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => p_0_in,
      Q => edge_detected,
      R => '0'
    );
\tmp_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(0),
      Q => tmp(0),
      R => '0'
    );
\tmp_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(10),
      Q => tmp(10),
      R => '0'
    );
\tmp_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(11),
      Q => tmp(11),
      R => '0'
    );
\tmp_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(12),
      Q => tmp(12),
      R => '0'
    );
\tmp_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(13),
      Q => tmp(13),
      R => '0'
    );
\tmp_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(14),
      Q => tmp(14),
      R => '0'
    );
\tmp_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(15),
      Q => tmp(15),
      R => '0'
    );
\tmp_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(16),
      Q => tmp(16),
      R => '0'
    );
\tmp_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(17),
      Q => tmp(17),
      R => '0'
    );
\tmp_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(18),
      Q => tmp(18),
      R => '0'
    );
\tmp_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(19),
      Q => tmp(19),
      R => '0'
    );
\tmp_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(1),
      Q => tmp(1),
      R => '0'
    );
\tmp_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(20),
      Q => tmp(20),
      R => '0'
    );
\tmp_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(21),
      Q => tmp(21),
      R => '0'
    );
\tmp_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(22),
      Q => tmp(22),
      R => '0'
    );
\tmp_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(23),
      Q => tmp(23),
      R => '0'
    );
\tmp_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(24),
      Q => tmp(24),
      R => '0'
    );
\tmp_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(25),
      Q => tmp(25),
      R => '0'
    );
\tmp_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(26),
      Q => tmp(26),
      R => '0'
    );
\tmp_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(27),
      Q => tmp(27),
      R => '0'
    );
\tmp_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(28),
      Q => tmp(28),
      R => '0'
    );
\tmp_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(29),
      Q => tmp(29),
      R => '0'
    );
\tmp_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(2),
      Q => tmp(2),
      R => '0'
    );
\tmp_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(30),
      Q => tmp(30),
      R => '0'
    );
\tmp_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(31),
      Q => tmp(31),
      R => '0'
    );
\tmp_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(3),
      Q => tmp(3),
      R => '0'
    );
\tmp_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(4),
      Q => tmp(4),
      R => '0'
    );
\tmp_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(5),
      Q => tmp(5),
      R => '0'
    );
\tmp_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(6),
      Q => tmp(6),
      R => '0'
    );
\tmp_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(7),
      Q => tmp(7),
      R => '0'
    );
\tmp_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(8),
      Q => tmp(8),
      R => '0'
    );
\tmp_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => din(9),
      Q => tmp(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_edge_detect_1_0 is
  port (
    clk : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    edge_detected : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_edge_detect_1_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_edge_detect_1_0 : entity is "design_1_edge_detect_1_0,edge_detect,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_edge_detect_1_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_edge_detect_1_0 : entity is "edge_detect,Vivado 2018.1";
end design_1_edge_detect_1_0;

architecture STRUCTURE of design_1_edge_detect_1_0 is
begin
inst: entity work.design_1_edge_detect_1_0_edge_detect
     port map (
      clk => clk,
      din(31 downto 0) => din(31 downto 0),
      edge_detected => edge_detected
    );
end STRUCTURE;
