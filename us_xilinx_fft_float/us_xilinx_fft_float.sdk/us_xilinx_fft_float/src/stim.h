#include "cplx_data.h"

// Linear combination of 2 sine waves
cplx_data_t sig_two_sine_waves[FFT_MAX_NUM_PTS] =
{
		{0.0, 0.0},{0.0, 1.0},{1.0, 0.0},{1.0, 1.0},{1.0, 1.0},{1.0, 0.0},{0.0, 1.0},{0.0, 0.0}
};
