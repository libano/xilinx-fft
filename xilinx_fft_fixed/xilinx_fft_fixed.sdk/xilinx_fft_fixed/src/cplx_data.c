
// Includes
#include <stdio.h>
#include "cplx_data.h"

float fixed_to_float(int fixed)
{
	return (((float)fixed)/((float)(1 << (16-2))))/2.0;
}

// Public functions
void cplx_data_get_string(char* c, cplx_data_t data, int length, int isForward, int isResult)
{
	float multiplier = 4.0;
	if(length > 8)
		multiplier = multiplier*multiplier;

	if(isResult)
		if(isForward) //forward
			sprintf(c, "%f + j*%f", fixed_to_float(data.data_re)*multiplier, fixed_to_float(data.data_im)*multiplier);
		else //inverse
			sprintf(c, "%f + j*%f", (fixed_to_float(data.data_re)/((float)length))*multiplier, (fixed_to_float(data.data_im)/((float)length))*multiplier);
	else
		sprintf(c, "%f + j*%f", fixed_to_float(data.data_re), fixed_to_float(data.data_im));
}
