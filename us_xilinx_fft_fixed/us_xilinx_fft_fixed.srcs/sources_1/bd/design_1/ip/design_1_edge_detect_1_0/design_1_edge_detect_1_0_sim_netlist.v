// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Tue Nov 13 11:33:33 2018
// Host        : Shears running 64-bit Ubuntu 16.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_1_edge_detect_1_0 -prefix
//               design_1_edge_detect_1_0_ design_1_edge_detect_1_0_sim_netlist.v
// Design      : design_1_edge_detect_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu9eg-ffvb1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_edge_detect_1_0,edge_detect,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "edge_detect,Vivado 2018.1" *) 
(* NotValidForBitStream *)
module design_1_edge_detect_1_0
   (clk,
    din,
    edge_detected);
  input clk;
  input [31:0]din;
  output edge_detected;

  wire clk;
  wire [31:0]din;
  wire edge_detected;

  design_1_edge_detect_1_0_edge_detect inst
       (.clk(clk),
        .din(din),
        .edge_detected(edge_detected));
endmodule

module design_1_edge_detect_1_0_edge_detect
   (edge_detected,
    din,
    clk);
  output edge_detected;
  input [31:0]din;
  input clk;

  wire clk;
  wire [31:0]din;
  wire edge_detected;
  wire edge_detected_i_i_10_n_0;
  wire edge_detected_i_i_11_n_0;
  wire edge_detected_i_i_12_n_0;
  wire edge_detected_i_i_13_n_0;
  wire edge_detected_i_i_2_n_0;
  wire edge_detected_i_i_3_n_0;
  wire edge_detected_i_i_4_n_0;
  wire edge_detected_i_i_5_n_0;
  wire edge_detected_i_i_6_n_0;
  wire edge_detected_i_i_7_n_0;
  wire edge_detected_i_i_8_n_0;
  wire edge_detected_i_i_9_n_0;
  wire p_0_in;
  wire [31:0]tmp;

  LUT4 #(
    .INIT(16'hFFFE)) 
    edge_detected_i_i_1
       (.I0(edge_detected_i_i_2_n_0),
        .I1(edge_detected_i_i_3_n_0),
        .I2(edge_detected_i_i_4_n_0),
        .I3(edge_detected_i_i_5_n_0),
        .O(p_0_in));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_10
       (.I0(tmp[12]),
        .I1(din[12]),
        .I2(din[14]),
        .I3(tmp[14]),
        .I4(din[13]),
        .I5(tmp[13]),
        .O(edge_detected_i_i_10_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_11
       (.I0(tmp[15]),
        .I1(din[15]),
        .I2(din[17]),
        .I3(tmp[17]),
        .I4(din[16]),
        .I5(tmp[16]),
        .O(edge_detected_i_i_11_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_12
       (.I0(tmp[3]),
        .I1(din[3]),
        .I2(din[5]),
        .I3(tmp[5]),
        .I4(din[4]),
        .I5(tmp[4]),
        .O(edge_detected_i_i_12_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_13
       (.I0(tmp[0]),
        .I1(din[0]),
        .I2(din[2]),
        .I3(tmp[2]),
        .I4(din[1]),
        .I5(tmp[1]),
        .O(edge_detected_i_i_13_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_2
       (.I0(tmp[27]),
        .I1(din[27]),
        .I2(din[29]),
        .I3(tmp[29]),
        .I4(din[28]),
        .I5(tmp[28]),
        .O(edge_detected_i_i_2_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_3
       (.I0(tmp[24]),
        .I1(din[24]),
        .I2(din[26]),
        .I3(tmp[26]),
        .I4(din[25]),
        .I5(tmp[25]),
        .O(edge_detected_i_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    edge_detected_i_i_4
       (.I0(edge_detected_i_i_6_n_0),
        .I1(edge_detected_i_i_7_n_0),
        .I2(edge_detected_i_i_8_n_0),
        .I3(edge_detected_i_i_9_n_0),
        .I4(edge_detected_i_i_10_n_0),
        .I5(edge_detected_i_i_11_n_0),
        .O(edge_detected_i_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF6FF6)) 
    edge_detected_i_i_5
       (.I0(din[31]),
        .I1(tmp[31]),
        .I2(din[30]),
        .I3(tmp[30]),
        .I4(edge_detected_i_i_12_n_0),
        .I5(edge_detected_i_i_13_n_0),
        .O(edge_detected_i_i_5_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_6
       (.I0(tmp[9]),
        .I1(din[9]),
        .I2(din[11]),
        .I3(tmp[11]),
        .I4(din[10]),
        .I5(tmp[10]),
        .O(edge_detected_i_i_6_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_7
       (.I0(tmp[6]),
        .I1(din[6]),
        .I2(din[8]),
        .I3(tmp[8]),
        .I4(din[7]),
        .I5(tmp[7]),
        .O(edge_detected_i_i_7_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_8
       (.I0(tmp[18]),
        .I1(din[18]),
        .I2(din[20]),
        .I3(tmp[20]),
        .I4(din[19]),
        .I5(tmp[19]),
        .O(edge_detected_i_i_8_n_0));
  LUT6 #(
    .INIT(64'h6FF6FFFFFFFF6FF6)) 
    edge_detected_i_i_9
       (.I0(tmp[21]),
        .I1(din[21]),
        .I2(din[23]),
        .I3(tmp[23]),
        .I4(din[22]),
        .I5(tmp[22]),
        .O(edge_detected_i_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    edge_detected_i_reg
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in),
        .Q(edge_detected),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(din[0]),
        .Q(tmp[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(din[10]),
        .Q(tmp[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(din[11]),
        .Q(tmp[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(din[12]),
        .Q(tmp[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(din[13]),
        .Q(tmp[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(din[14]),
        .Q(tmp[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(din[15]),
        .Q(tmp[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(din[16]),
        .Q(tmp[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(din[17]),
        .Q(tmp[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(din[18]),
        .Q(tmp[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(din[19]),
        .Q(tmp[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(din[1]),
        .Q(tmp[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(din[20]),
        .Q(tmp[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(din[21]),
        .Q(tmp[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(din[22]),
        .Q(tmp[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(din[23]),
        .Q(tmp[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(din[24]),
        .Q(tmp[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(din[25]),
        .Q(tmp[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(din[26]),
        .Q(tmp[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(din[27]),
        .Q(tmp[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(din[28]),
        .Q(tmp[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(din[29]),
        .Q(tmp[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(din[2]),
        .Q(tmp[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(din[30]),
        .Q(tmp[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(din[31]),
        .Q(tmp[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(din[3]),
        .Q(tmp[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(din[4]),
        .Q(tmp[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(din[5]),
        .Q(tmp[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(din[6]),
        .Q(tmp[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(din[7]),
        .Q(tmp[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(din[8]),
        .Q(tmp[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(din[9]),
        .Q(tmp[9]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
