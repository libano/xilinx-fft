
// Includes
#include <stdio.h>
#include "cplx_data.h"

// Public functions
void cplx_data_get_string(char* c, cplx_data_t data, int length, int isForward, int isResult)
{
	if(isResult && !isForward) //inverse
		sprintf(c, "%f + j*%f", (data.data_re/((float)length)), (data.data_im/((float)length)));
	else
		sprintf(c, "%f + j*%f", data.data_re, data.data_im);
}
